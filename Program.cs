﻿using System;
using MySql.Data.MySqlClient;
namespace BilgisayarYonetimiTesting
{
    class Veritabani {
        public static MySqlConnection _Connection { get; set; }
        public static MySqlDataReader _Reader { get; set; }
        public static MySqlCommand _Command { get; set; }

        public Veritabani()
        {
            _Connection = new MySqlConnection("Server=localhost; Database = test; Uid=root; Pwd=; SslMode=None");
        }
        public Veritabani(string veritabani, string host, string kullanici, string parola)
        {
            _Connection = new MySqlConnection("Server=" + host + "; Database = " + veritabani + "; Uid=" + kullanici + "; Pwd=" + parola + "; SslMode=None");
        }

        public static bool Execute() {
            int s;
            bool Result = false;
            s = _Command.ExecuteNonQuery();
            if (s > 0)
            {
                Result = true;
            }
            else {
                Result = false;
            }
            return Result;
        }
        public static void Reader() {
            _Reader = _Command.ExecuteReader();
        }
        public static void AddWithValue(string key, string parameter) {
            _Command.Parameters.AddWithValue(key, parameter);
        }

        public static void Command(string command_text)
        {
            _Command = new MySqlCommand();
            _Command.CommandText = command_text;
        }

        public static void CommandSetConnection() {
            _Command.Connection = _Connection;
        }

        public static void ConnectionOpen() {
            _Connection.Open();
        }
        public static void ConnectionClose() {
            _Command.Dispose();
            _Reader.Close();
            _Reader.Dispose();
            _Connection.Close();
            _Connection.Dispose();
        }


        public static void Truncate(string TabloAdi) {
            ConnectionOpen();
            Command("TRUNCATE TABLE " + TabloAdi);
            CommandSetConnection();
            Execute();
            ConnectionClose();
        }
    }
    class Testing  : Veritabani {
        public static void TestQuery() {
            ConnectionOpen();
            Command("SELECT * FROM pc_state");
            CommandSetConnection();
            Reader();
            while (_Reader.Read())
            {
                Console.WriteLine(_Reader["State"].ToString());
            }
            ConnectionClose();
        }

        public static void TestInsert() {
            ConnectionOpen();
            Command("insert into messages(message_title, message) values (@title, @message)");
            CommandSetConnection();
            AddWithValue("title", "Veritabani test");
            AddWithValue("message", "veritabanı test mesajı");
            bool Exec = Execute();
            if (Exec)
            {
                Console.WriteLine("Test Insert Success");
            }
            else {
                Console.WriteLine("Hatalı işlem");
            }
            ConnectionClose();
        }

        public static void TestTruncate()
        {
            ConnectionOpen();

            ConnectionClose();
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Testing t = new Testing();

            Testing.TestQuery();
            Testing.TestInsert();
            Console.Read();
        }
    }
}
